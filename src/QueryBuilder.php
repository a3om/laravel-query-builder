<?php

namespace A3om\QueryBuilder;

class QueryBuilder extends \Illuminate\Database\Eloquent\Builder
{
    public function get($columns = ['*'])
    {
        return parent::get($columns);
    }

    public function first($columns = ['*'])
    {
        return parent::first($columns);
    }

    public function last($columns = ['*'])
    {
        return parent::last($columns);
    }

    public function paginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null)
    {
        return parent::paginate($perPage, $columns, $pageName, $page);
    }

    public function simplePaginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null)
    {
        return parent::simplePaginate($perPage, $columns, $pageName, $page);
    }
}
